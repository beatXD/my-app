
# build stage
FROM node:16.14.0 as builder
WORKDIR /app

COPY package*.json ./
COPY . .
RUN yarn install
RUN yarn build

# deploy stage
FROM node:16.14.0 as runner
WORKDIR /app
COPY --from=builder /app  .

ENV HOST 0.0.0.0
ENV PORT 3000

EXPOSE 3000

CMD [ "yarn", "start" ]
